
class Student implements Cloneable{
	int age;
	String name;
	
	public Student(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Student [age=" + age + ", name=" + name + "]";
	}

	protected Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
}

public class CloneableInterface {

	public static void main(String[] args) throws CloneNotSupportedException {
		Student s1 = new Student(10, "bolobolo");
		System.out.println(s1);
		
		Student s2 = (Student) s1.clone();
		System.out.println(s2);
		
		System.out.println(s1.age);
		System.out.println(s1.name);

	}

}
